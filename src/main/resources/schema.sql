-- WARNING RECREATES ALL SCHEMAS
-- DOES NOT MAKE SENSE TO USE IN PRODUCTION.
-- TODO: include a production config
-- drop table if exists guild_settings;

create table if not exists guild_settings (
    guild_id varchar(255) primary key not null,
    msg_cmd_prefix varchar(20)
);


create table if not exists hoyo_creds (
    id character varying(255) not null primary key,
    account_id_v2 character varying(255),
    account_mid_v2 character varying(255),
    cookie_token_v2 character varying(255),
    password_enc character varying(1024),
    require_re_auth boolean not null default true,
    username_enc character varying(1024)
);


create table if not exists hoyo_details (
    id character varying(255) not null primary key,
    hoyo_creds_id character varying (255),
    constraint fk_hoyo_creds_id
    foreign key (hoyo_creds_id) references hoyo_creds
);

create table if not exists red_code (
    code character varying (255) not null primary key,
    priority integer not null,
    queued_for_all boolean not null
);

create table if not exists uid
(
    uid_number character varying(255) not null
        primary key,
    game integer,
    hoyo_details_id character varying(255),
    region integer
);

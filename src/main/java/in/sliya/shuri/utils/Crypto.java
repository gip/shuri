package in.sliya.shuri.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class Crypto {

    static final Logger logger = LoggerFactory.getLogger(Crypto.class);

    public static PublicKey parsePublicKey(String publicKeyPEM)  {
        // Remove the PEM header and footer
        String publicKeyPEMContent = publicKeyPEM
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replace("-----END PUBLIC KEY-----", "")
                .replaceAll("\\s", ""); // Remove all whitespaces including newline characters

        // Decode the Base64 encoded public key
        byte[] publicKeyBytes = Base64.getDecoder().decode(publicKeyPEMContent);

        // Generate PublicKey object from the decoded bytes
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");

            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
            return keyFactory.generatePublic(keySpec);

        } catch (Exception e) {
            logger.error("exception while loading public key to encrypt message", e);
        }

        // TODO: FIX THIS!!
        return null;

    }

    public static String encryptMessage(String message, PublicKey publicKey) {

        try {

            javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance("RSA");

            // Initialize the cipher for encryption using the public key
            cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, publicKey);

            // Encrypt the message
            byte[] encryptedBytes = cipher.doFinal(message.getBytes());

            // Convert encrypted bytes to Base64 encoded string
            return Base64.getEncoder().encodeToString(encryptedBytes);
        } catch (Exception e) {
            logger.error("exception while encrypting message", e);

        }

        // TODO: FIX THIS!!
        return null;

    }
}

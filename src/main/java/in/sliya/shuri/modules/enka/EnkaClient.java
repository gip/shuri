package in.sliya.shuri.modules.enka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;

@Component
public class EnkaClient {

    private static final String ENKA_ENDPOINT = "https://enka.network/api";

    @Autowired
    Client client;
    boolean validateUidNum(String uidNum) {
        WebTarget target  = client.target(ENKA_ENDPOINT)
                .path("/uid/" + uidNum)
                .queryParam("info", "");
        try {
            System.out.println(target.getUri().toURL().toString());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON);
                Response response = builder.get();

        return response.getStatus() == 200;


   }


}

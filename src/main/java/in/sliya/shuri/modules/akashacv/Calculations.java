package in.sliya.shuri.modules.akashacv;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Calculations {
    // TODO: make this a long, which is parsed from string to and fro
    String ranking;
    Double outOf;


    @JsonProperty("fit")
    private void fit(Map<String, Object> map) {
        this.ranking = (String) map.get("ranking");
        this.outOf = ((Number) map.get("outOf")).doubleValue();
    }
    public byte getRanking() {
        return (byte) Math.ceil(100 * (
                Double.parseDouble(ranking.substring(1)) /
                outOf
        ));


    }

}

package in.sliya.shuri.modules.akashacv;

import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

@Component
public class AkashCV {

    Client client;


    AkashCV() {
        client = ClientBuilder.newClient();
    }


    public  UserCalculation getUserCalculation(String uid) {
        WebTarget target = client.target("https://akasha.cv/api").path(String.format("/getCalculationsForUser/%s", uid));

        Invocation invocation = target.request(MediaType.APPLICATION_JSON).buildGet();
        return  invocation.invoke(new GenericType<UserCalculation>(){});
    }



}

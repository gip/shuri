package in.sliya.shuri.modules.genshin.model;

import java.util.Objects;

public enum Game {
    GENSHIN,
    STARRAIL,
    UNKNOWN;

    public static Game fromBiz(String gameBiz) {
       if (Objects.equals(gameBiz, "hk4e_global")) return Game.GENSHIN;
       else if (Objects.equals(gameBiz, "hkrpg_global")) return Game.STARRAIL;
       else return Game.UNKNOWN;
    }

    public static Game from_str(String gameName) {
        if (gameName.equals("genshin")) return Game.GENSHIN;
        else if (gameName.equals("star_rail")) return Game.STARRAIL;
        else return Game.UNKNOWN;
    }
}

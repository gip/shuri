package in.sliya.shuri.modules.genshin;

import in.sliya.shuri.modules.genshin.model.Region;

public class Utils {

    public static Region findRegionByUid(String uid) {

        if (uid.startsWith("8")) return Region.ASIA;
        if (uid.startsWith("7")) return Region.EUROPE;
        else return Region.UNKNOWN;
    }
}

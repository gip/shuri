package in.sliya.shuri.modules.genshin.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;


@NoArgsConstructor
@Entity
@Data
public class Uid  {

    String hoyoDetailsId;

    @Id
    String uidNumber;

    Region region;

    Game game;

    public Uid(String uidNumber, Game game) {
        this.uidNumber = uidNumber;
        region = getRegionByUid(uidNumber);
        this.game = game;

        if (region == Region.UNKNOWN) {
            throw new InvalidUidException("provided uid invalid: " + uidNumber);
        }
    }

    public static Region getRegionByUid(String uidNumber) {

        if (uidNumber.startsWith("8")) return Region.ASIA;
        if (uidNumber.startsWith("7")) return Region.EUROPE;
        else return Region.UNKNOWN;
    }

    public static class InvalidUidException extends RuntimeException {
        InvalidUidException(String message) {
            super(message);
        }
    }


}






package in.sliya.shuri.modules.genshin.model;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Data
@NoArgsConstructor
public class HoyoDetails {

    @Id
    String id;

//    @MapKey(name="region")
//    Map<Region, Uid> uidList = new HashMap<>();

    @OneToMany(mappedBy = "hoyoDetailsId", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    List<Uid> uidList = new ArrayList<>();

    @OneToOne
    HoyoCreds hoyoCreds;

    public HoyoDetails(String id, Uid uid, Game game) {
       this.id = id;
       this.putUid(uid);
    }

    public void putUid(Uid uid) {
        this.uidList.add(uid);
    }


}



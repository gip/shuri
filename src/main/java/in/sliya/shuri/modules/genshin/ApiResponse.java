package in.sliya.shuri.modules.genshin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class ApiResponse<Data> {
    public int retcode;
    public String message;
    public Data data;
}

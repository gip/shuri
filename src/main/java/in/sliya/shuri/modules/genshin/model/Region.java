package in.sliya.shuri.modules.genshin.model;

import lombok.Getter;

@Getter
public enum Region {
    ASIA,
    EUROPE,
    UNKNOWN;

}

package in.sliya.shuri.modules.genshin.model;

import in.sliya.shuri.utils.Crypto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.security.PublicKey;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class HoyoCreds {



    @Id
    String id;

    String cookie_token_v2;
    String account_id_v2;
    String account_mid_v2;

    boolean requireReAuth;

    @Column(length = 1024)
    String usernameEnc;

    @Column(length = 1024)
    String passwordEnc;


    public HoyoCreds(String id, String username, String password) {

        String publicKeyPEM = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4PMS2JVMwBsOIrYWRluYwEiFZL7Aphtm9z5Eu/anzJ09nB00uhW+ScrDWFECPwpQto/GlOJYCUwVM/raQpAj/xvcjK5tNVzzK94mhk+j9RiQ+aWHaTXmOgurhxSp3YbwlRDvOgcq5yPiTz0+kSeKZJcGeJ95bvJ+hJ/UMP0Zx2qB5PElZmiKvfiNqVUk8A8oxLJdBB5eCpqWV6CUqDKQKSQP4sM0mZvQ1Sr4UcACVcYgYnCbTZMWhJTWkrNXqI8TMomekgny3y+d6NX/cFa66jozFIF4HCX5aW8bp8C8vq2tFvFbleQ/Q3CU56EWWKMrOcpmFtRmC18s9biZBVR/8QIDAQAB";

        // TODO: load this from file.
        PublicKey publicKey = Crypto.parsePublicKey(publicKeyPEM);

        this.id = id;
        this.usernameEnc =  Crypto.encryptMessage(username, publicKey);
        this.passwordEnc =  Crypto.encryptMessage(password, publicKey);
        this.requireReAuth = false;
    }

    public String buildCookie() {
        return "account_id_v2" + "=" + this.getAccount_id_v2() + "; " +
                "account_mid_v2" + "=" + this.getAccount_mid_v2() + "; " +
                "cookie_token_v2" + "=" + this.getCookie_token_v2() + "; ";
    }

}

package in.sliya.shuri.modules.genshin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import in.sliya.shuri.modules.genshin.model.Game;
import in.sliya.shuri.modules.genshin.model.Uid;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class GeneralApiClient {

    @Autowired
    Client client;
    private final String UID_LIST_URL = "https://api-account-os.hoyoverse.com/account/binding/api/getUserGameRolesByCookieToken";

    // TODO: needs rework here after making Game model  a part of Uid model.
    Map<Game, List<Uid>> getUidList() {
        Response response = client.target(UID_LIST_URL)
                .request(MediaType.APPLICATION_JSON)
                .header("Cookie", "account_id_v2=76762900; account_mid_v2=11lfp9545f_hy; cookie_token_v2=v2_CAQSDGNlMXRidXdiMDB6ayCx9eetBij3_NudAzCUns0kQgtoazRlX2dsb2JhbA")
                .get();
        ApiResponse<UserGameRoles> responseBody = response.readEntity(new GenericType<>() {});

        Map<Game, List<Uid>> uidList = new HashMap<>();

        for (UserGameRole role : responseBody.data.list) {
            Game game = Game.fromBiz(role.game_biz);
            if (game == Game.UNKNOWN) continue;
            uidList.computeIfAbsent(game, k -> new ArrayList<>()).add(new Uid(role.game_uid, game));
        }
        return uidList;
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    @ToString
    public static class UserGameRoles {
        public List<UserGameRole> list;



    }

    public static class UserGameRole {
        public String game_biz;
        public String game_uid;
        public String nickname;
        public int level;
        public boolean is_chosen;
        public String region_name;
        public boolean is_official;
        public String region;
    }
}

package in.sliya.shuri.modules.genshin;

import in.sliya.shuri.modules.genshin.model.Game;
import in.sliya.shuri.modules.genshin.model.HoyoDetails;
import in.sliya.shuri.modules.genshin.model.Uid;
import in.sliya.shuri.modules.genshin.repo.HoyoDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Objects;
import java.util.Optional;

@Repository
public class RegisterUid {

    @Autowired
    HoyoDetailsRepo repo;


    @Transactional
    public String registerUid(String userId, String uidNumber, String game) {
        Uid newUid = null;
        try {
            newUid= new Uid(uidNumber, Game.from_str(game));
            newUid.setHoyoDetailsId(userId);
        } catch (Uid.InvalidUidException e) {
            return "Invalid UID";
        }

        Optional<HoyoDetails> hoyoDetails = repo.findById(userId);
        if (hoyoDetails.isEmpty()) {
            hoyoDetails = Optional.of(new HoyoDetails(userId, newUid, Game.from_str(game)));
            repo.save(hoyoDetails.get());
            return "New UID registered";
        } else {
            Uid finalNewUid = newUid;
            Optional<Uid> toRemove = hoyoDetails.get().getUidList().stream().filter(u -> {
                return u.getGame() == finalNewUid.getGame() && u.getRegion() == finalNewUid.getRegion();
            }).findFirst();

            if (toRemove.isPresent()) {
                // firstly if they're same uids just say so.
                if (Objects.equals(toRemove.get().getUidNumber(), uidNumber)) {
                    return "UID already exists";
                }
                hoyoDetails.get().getUidList().remove(toRemove.get());

            }

            hoyoDetails.get().getUidList().add(newUid);
            return "Registered/Updated Succesfully.";
        }
    }
}

package in.sliya.shuri.modules.genshin.repo;

import in.sliya.shuri.modules.genshin.model.HoyoDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface HoyoDetailsRepo extends CrudRepository<HoyoDetails, String> {

}

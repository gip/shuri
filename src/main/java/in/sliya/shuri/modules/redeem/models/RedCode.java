package in.sliya.shuri.modules.redeem.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class RedCode {
    @Id
    String code;
    boolean queuedForAll;
    int priority;
}

package in.sliya.shuri.modules.redeem;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.sliya.shuri.modules.genshin.ApiResponse;
import in.sliya.shuri.modules.genshin.model.Region;
import in.sliya.shuri.modules.genshin.model.HoyoCreds;
import in.sliya.shuri.modules.genshin.model.HoyoDetails;
import in.sliya.shuri.modules.redeem.dao.HoyoCredsDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

import static in.sliya.shuri.modules.genshin.Utils.findRegionByUid;

@Component
public class RedApiClient {

    private static final String WEBLOGIN_URL = "https://sg-public-api.hoyoverse.com/account/ma-passport/api/webLoginByPassword";
    private static final String REDEEM_URL= "https://sg-hk4e-api.hoyoverse.com/common/apicdkey/api/webExchangeCdkey";

    static final Logger logger = LoggerFactory.getLogger(RedApiClient.class);

    @Autowired
    HoyoCredsDao dao;


    RedStatus redeemCode(HoyoDetails hoyoDetails, String code, Region rxegion) {

        HoyoCreds credentials = null;
        if (credentials == null) {
            throw new RuntimeException("no credentials found for userId: " );
        }

        logger.info("validating credentials");
        if (!validateCreds(credentials)) {
            logger.info("refreshing.. because not valid");
            refreshCookies(credentials);
        }

       // TODO: find uid for credentials.
       String uid = "812786494";

        String region = "";
        if (findRegionByUid(uid) == Region.ASIA) {
            region = "os_asia";
        } else if (findRegionByUid(uid) == Region.EUROPE) {
            region = "os_europe";
        } else {
            // TODO: why? add uid validation already.
            throw new RuntimeException("unknown region");
        }


        final String lang = "en";
        final String game_biz = "hk4e_global";
        final String sLangKey = "en-us";



        Client client = ClientBuilder.newClient();
        Invocation.Builder builder = client.target(REDEEM_URL)
                .queryParam("uid", uid)
                .queryParam("region", region)
                .queryParam("lang",lang)
                .queryParam("cdkey", code)
                .queryParam("game_biz", game_biz)
                .queryParam("sLangKey", sLangKey)
                .request(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en-GB,en;q=0.9")
                .header("Connection", "keep-alive")
                .header("Origin", "https://genshin.hoyoverse.com")
                .header("Referer", "https://genshin.hoyoverse.com/")
                .header("Cookie", credentials.buildCookie());



        Response response = builder.get();
        ApiResponse apiResponse = response.readEntity(ApiResponse.class);

        if (response.getStatus() == 200) {
            return RedStatus.getRedeemStatus(apiResponse.retcode);
        } else {
            throw new RuntimeException("Non HTTP OK response from API. code: " + response.getStatus() + " apiResponse:" + apiResponse);
        }

    }



    void refreshCookies(HoyoCreds credentials)  {

        Map<String, String> accountData = new HashMap<>();
        accountData.put("account", credentials.getUsernameEnc());
        accountData.put("password", credentials.getPasswordEnc());
        accountData.put("token_type", "4");



        String payload;
        try {
            payload = new ObjectMapper().writeValueAsString(accountData);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("error parsing payload json for refresh.", e);
        }


        Client client = ClientBuilder.newClient();
        Response response = client.target(WEBLOGIN_URL)
                .request(MediaType.APPLICATION_JSON)
                .header("Accept-Language", "en-GB,en;q=0.9")
                .header("Connection", "keep-alive")
                .header("Origin", "https://account.hoyoverse.com")
                .header("Referer", "https://account.hoyoverse.com/")
                .header("x-rpc-app_id", "ce1tbuwb00zk")
                .post(Entity.entity(payload, MediaType.APPLICATION_JSON));

        ApiResponse responseBody = response.readEntity(ApiResponse.class);

        response.close();
        client.close();

        if (response.getStatus() == 200) {
            if (responseBody.retcode == 0) {
                Map<String, NewCookie> cookies = response.getCookies();
                try {
                    credentials.setAccount_id_v2(cookies.get("account_id_v2").getValue());
                    credentials.setAccount_mid_v2(cookies.get("account_mid_v2").getValue());
                    credentials.setCookie_token_v2(cookies.get("cookie_token_v2").getValue());
                    dao.saveCreds(credentials);

                } catch (NullPointerException e) {
                    throw new RuntimeException("unknown error occurred getting cookies, consider refactoring this code.");
                }
                logger.info("fresh valid tokens saved");


            } else if (responseBody.retcode == -3101) {
                throw new CaptchaOrWrongAuthException("verification needed try again later: " + responseBody);
            }
        } else {
            throw new RuntimeException("Non HTTP OK response from API. code: " + response.getStatus() + " body:" + responseBody);
        }

    }



    static boolean validateCreds(HoyoCreds creds) {
        // TODO: also use API
        return creds.getAccount_id_v2() != null;
    }


    public static class CaptchaOrWrongAuthException extends RuntimeException {
        CaptchaOrWrongAuthException(String message) {
            super(message);
        }
    }




}


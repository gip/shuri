package in.sliya.shuri.modules.redeem;

public enum RedStatus {

    USED,
    REDEEMED,
    INVALID,
    EXPIRED,
    COOLDOWN,
    UNKNOWN;

    public static RedStatus getRedeemStatus(int retcode) {
        if (retcode == 0) return REDEEMED;
        else if (retcode == -2017) return USED;
        else if (retcode == -2003) return INVALID;
        else if (retcode == -2001) return EXPIRED;
        else if (retcode == -2016) return COOLDOWN;
        else return UNKNOWN;
    }
}

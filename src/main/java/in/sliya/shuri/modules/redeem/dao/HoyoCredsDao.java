package in.sliya.shuri.modules.redeem.dao;

import in.sliya.shuri.modules.genshin.model.HoyoCreds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class HoyoCredsDao {



    @Autowired
    EntityManager em;


    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public HoyoCreds saveCreds(HoyoCreds credentials) {

        if (em.find(HoyoCreds.class, credentials.getId()) !=  null) {
            return em.merge(credentials);
        } else {
            em.persist(credentials);
            return credentials;
        }
    }

    public HoyoCreds getCredsById(String userId) {
        return em.find(HoyoCreds.class, userId);
    }




}




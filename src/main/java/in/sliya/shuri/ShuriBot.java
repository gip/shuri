package in.sliya.shuri;

import in.sliya.shuri.base.SlashCmdListener;
import in.sliya.shuri.commands.UidCmd;
import in.sliya.shuri.commands.redeem.ContinueRegisterUser;
import in.sliya.shuri.config.ShuriConfig;
import in.sliya.shuri.database.dao.GuildSettingsDao;
import in.sliya.shuri.base.MsgCmdListener;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.exceptions.InvalidTokenException;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.EnumSet;

@SpringBootApplication
public class ShuriBot implements CommandLineRunner {

	static final Logger logger = LoggerFactory.getLogger(ShuriBot.class);
	@Autowired
	MsgCmdListener msgCmdListener;
	@Autowired
	SlashCmdListener slashCmdListener;

	@Autowired
	UidCmd uidCmd;

	ShuriConfig config;
	GuildSettingsDao guildSettings;

	@Autowired
	public void setConfig(ShuriConfig config) {
		this.config = config;
	}

	@Autowired
	public void setGuildSettings(GuildSettingsDao guildSettings) {
		this.guildSettings = guildSettings;
	}

	ShuriBot() {
		logger.info("booting");
	}

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(ShuriBot.class, args);

	}




	@Override
	public void run(String... args) throws Exception {
//		if(true) return;

		logger.info("settings!: {}", guildSettings.getById("794127125871198240"));

		EnumSet<GatewayIntent> intents = EnumSet.of(
				GatewayIntent.GUILD_MESSAGES,
				GatewayIntent.DIRECT_MESSAGES,
				GatewayIntent.MESSAGE_CONTENT,
				GatewayIntent.GUILD_MESSAGE_REACTIONS,
				GatewayIntent.DIRECT_MESSAGE_REACTIONS
		); // TODO: why using enumset here?

		try {

			// TODO: make this managed.
			JDA jda = JDABuilder.createLight(config.getBotToken(), intents)
					.addEventListeners(msgCmdListener)
					.addEventListeners(slashCmdListener)
					.addEventListeners(uidCmd)
					.setActivity(Activity.listening("watching you..."))
					.build();

			jda.getRestPing().queue(ping -> System.out.println("logged in with ping: " + ping));
			jda.awaitReady();


			logger.info("{} is in {} guilds", config.getBotName(),  jda.getGuildCache().size());

			postProcess(jda);


		} catch (InvalidTokenException e) {
			logger.error("provided bot token could be invalid, exiting.");
		}
	}

	private void postProcess(JDA jda){

		// logger.info("perms: {}", jda.getGuildById("889090539620814848").getMemberById(jda.getSelfUser().getId()).getPermissions());

		// register slash commands for GIP
		jda.getGuildById("889090539620814848").updateCommands().addCommands(
//			Commands.slash("register", "Register Genshin Account"),
			Commands.slash("uid", "link uid")
					.addOptions(
							new OptionData(OptionType.STRING, "game", "Game", true)
									.addChoice("Star Rail", "star_rail")
									.addChoice("Genshin", "genshin"),
							new OptionData(OptionType.STRING, "uid", "UID to link", true)
					),
			Commands.slash("cards", "view character cards")

		).queue();

	}

}

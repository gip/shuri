package in.sliya.shuri.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "shuri")
public class ShuriConfig {
    private String botToken;
    private String awsAccessKey;
    private String awsAccessKeySecret;
    private String botName;
    private String defaultPrefix;


}

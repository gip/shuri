package in.sliya.shuri.config;

import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

@Configuration
@ConfigurationPropertiesScan("in.sliya.shuri.config")
@PropertySources({
        @PropertySource(value = "classpath:shuri.properties")
})
public class Setup {

    @Bean
    public Client jeresyClient() {
        return ClientBuilder.newClient();
    }
}

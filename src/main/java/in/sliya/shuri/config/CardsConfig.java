package in.sliya.shuri.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "shuri.cards")
public class CardsConfig {

    private String uidList;
    private String dataFolder;
}

package in.sliya.shuri.database.dao;


import in.sliya.shuri.database.model.GuildSettings;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class GuildSettingsDao {


    @PersistenceContext
    EntityManager em;


    public GuildSettings getById(String guildId) {
        return em.find(GuildSettings.class, guildId);
    }

    public void createSettings(GuildSettings settings) {
        em.persist(settings);
    }

}

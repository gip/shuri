package in.sliya.shuri.database.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GuildSettings {
    @Id
    private String guildId;
    private String msgCmdPrefix;

}

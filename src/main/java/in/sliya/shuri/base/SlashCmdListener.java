package in.sliya.shuri.base;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class SlashCmdListener extends ListenerAdapter {

    private final static Logger logger = LoggerFactory.getLogger(SlashCmdListener.class);
    private List<CmdHandler> handlers;


    @PostConstruct
    public void init() {
        logger.info("slash command listener ready. found slash {} handlers waiting for command.", handlers.size());
    }


    @Autowired
    public void setHandlers(List<CmdHandler> handlers) {
        this.handlers = handlers;
    }
    @Override
    public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
        handlers.forEach(cmd -> {
            if (event.getName().equals(cmd.getCommandName())) {
                logger.info("handler found for slash command: {}, invoking...", event.getName());
                logger.info("handling slash command interaction: {}", event.getName());
                cmd.executeSlashCmd(event);
            }
        });

    }


}

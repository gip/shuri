package in.sliya.shuri.base;
import lombok.Getter;
import java.util.List;

/**
 * Parsed legacy message style command with command name argument information.
 */
@Getter
public class MessageCommand {


    private final String rawArg; // entire message excluding the command prefix + command name + first space.

    public MessageCommand(String rawArg) {
        this.rawArg = rawArg;
    }

    /**
     * tokenize the argument with provided rules.
     * @return tokenized strings
     */
    public List<String> getTokens() {
       throw new RuntimeException("unimplemented");
    }

    /**
     * returns the raw argument with white spaces trimmed
     */
    public String getArg() {
       return rawArg.trim();
    }

    // TODO: add arguments retrieval. ex: "First" "and" "third" "argument" <- total 4 args.
}

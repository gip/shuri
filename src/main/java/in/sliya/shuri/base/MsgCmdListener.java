package in.sliya.shuri.base;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class MsgCmdListener extends ListenerAdapter {

    private final static Logger logger = LoggerFactory.getLogger(MsgCmdListener.class);
    private List<CmdHandler> handlers;


    @PostConstruct
    public void init() {
        logger.info("command listener ready. found message {} handlers waiting for command.", handlers.size());
    }


   @Autowired
   public void setHandlers(List<CmdHandler> handlers) {
        this.handlers = handlers;
   }

    @Override
    public void onMessageReceived( MessageReceivedEvent event) {
        logger.info("msg: {}", event.getMessage().getContentRaw());
        invokePossibleCmd(event);
    }

    private void invokePossibleCmd(@NotNull MessageReceivedEvent event) {

        // ignore all guilds except my guild; ignore bots.
        Guild guild = event.getGuild();
        if ((!guild.getId().equals("794127125871198240") && !event.getChannel().getId().equals("1201449069558628382")) || event.getAuthor().isBot() ) {
            return;
        }

        String prefix = "s.";
        String rawMessage = event.getMessage().getContentRaw();

        if (!rawMessage.startsWith(prefix)) return;
        // command invoked.

        int firstSpaceAt = rawMessage.indexOf(' ');
        String commandName = rawMessage.substring(
                prefix.length(),
                firstSpaceAt == -1 ? rawMessage.length() : firstSpaceAt
        );

        logger.debug("invoked possible command: {}", commandName);

        handlers.forEach(cmd -> {
            if (commandName.equals(cmd.getCommandName())) {
                logger.info("handler found for command: {}, invoking...", commandName);
                cmd.executeCmd(new MessageCommand(rawMessage.substring(firstSpaceAt + 1)), event);
            }
        });
    }
}

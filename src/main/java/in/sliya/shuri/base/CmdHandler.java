package in.sliya.shuri.base;

import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.UUID;

@Getter
@Setter
public abstract class CmdHandler {
   private String commandName;

   public void handleMsg(MessageCommand cmd, MessageReceivedEvent event) {
     throw new RuntimeException("unimplmented slash command: ");
   }
   public  void handleSlash(SlashCommandInteractionEvent event) {
      throw new RuntimeException("unimplemented slash command: " + event.getName());
   }

   public void executeCmd(MessageCommand cmd, MessageReceivedEvent event) {
      String traceId = UUID.randomUUID().toString();
      try {
         handleMsg(cmd, event);
      } catch (Exception e) {

         event.getChannel().sendMessage(String.format("Server error. check logs for more info. trace id: %s, exception:  ", traceId) + e.toString()).queue();
         throw e;
      }
   }


   public void executeSlashCmd(SlashCommandInteractionEvent event) {
      String traceId = UUID.randomUUID().toString();
      try {
         handleSlash(event);
      } catch (Exception e) {
         event.getChannel().sendMessage(String.format("Server error. check logs for more info. trace id: %s, exception:  ", traceId) + e.toString()).queue();
         throw e;
      }
   }
}

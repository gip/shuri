package in.sliya.shuri.commands;


import in.sliya.shuri.modules.akashacv.AkashCV;
import in.sliya.shuri.base.MessageCommand;
import in.sliya.shuri.base.CmdHandler;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


// TODO: this should be a system command to overthrow all delays and report much better ping.
@Component
public class AkashaCV extends CmdHandler {

    private final Logger logger = LoggerFactory.getLogger(AkashaCV.class);

    AkashaCV() {
        setCommandName("ak");
    }

    @Autowired
    AkashCV ak;

    @Override
    public void handleMsg(MessageCommand cmd, MessageReceivedEvent event) {

        String[] split = cmd.getArg().split(" ", 2);
        String uid = split[0];

        if (uid.length() == 0) {
            // need uid
        }

        if (!validateUid()) {
            // uid wrong
        }

//        String character = split[1];

        StringBuilder sb = new StringBuilder();

        ak.getUserCalculation(uid).getCharacterList().forEach(c -> {
            sb.append(String.format("Top %d%%: %s\n", c.calculations.getRanking(), c.name));
        });

        event.getChannel().sendMessage(sb.toString()).queue();
    }

    // TODO: decouple common functionality.
    private boolean validateUid() {
        return true;
    }



    @Override
    public void handleSlash(SlashCommandInteractionEvent event) {

    }
}

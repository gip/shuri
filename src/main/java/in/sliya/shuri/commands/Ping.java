package in.sliya.shuri.commands;


import in.sliya.shuri.base.MessageCommand;
import in.sliya.shuri.base.CmdHandler;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;


// TODO: this should be a system command to overthrow all delays and report much better ping.
@Component
public class Ping extends CmdHandler {

    private final Logger logger = LoggerFactory.getLogger(Ping.class);

    Ping() {
        setCommandName("ping");
    }

    @Override
    public void handleMsg(MessageCommand cmd, MessageReceivedEvent event) {
        long now = OffsetDateTime.now().toInstant().toEpochMilli();
        long diff = now -  event.getMessage().getTimeCreated().toInstant().toEpochMilli();
        event.getChannel().sendMessage(String.format("pong. took : %dms", diff)).queue();
    }
}

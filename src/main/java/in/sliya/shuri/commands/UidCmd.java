package in.sliya.shuri.commands;

import com.fasterxml.jackson.databind.ObjectMapper;
import in.sliya.shuri.base.CmdHandler;
import in.sliya.shuri.config.CardsConfig;
import in.sliya.shuri.modules.genshin.RegisterUid;
import in.sliya.shuri.modules.genshin.model.Game;
import in.sliya.shuri.modules.genshin.model.HoyoDetails;
import in.sliya.shuri.modules.genshin.model.Uid;
import in.sliya.shuri.modules.genshin.repo.HoyoDetailsRepo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.StringSelectInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.components.selections.SelectOption;
import net.dv8tion.jda.api.interactions.components.selections.StringSelectMenu;
import net.dv8tion.jda.api.utils.FileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
public class UidCmd extends ListenerAdapter  {

    @Autowired
    HoyoDetailsRepo hoyoDetailsRepo;

    private final Logger logger = LoggerFactory.getLogger(UidCmd.class);


    @Autowired
    CardsConfig cardsConfig;

    @Autowired
    RegisterUid registerUid;

    @Override
    @Transactional
    public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {

        String userId = event.getUser().getId();

        if (event.getName().equals("uid")) {

            String uidNumber  = event.getOption("uid").getAsString();
            String game = event.getOption("game").getAsString();

            event.reply(registerUid.registerUid(userId, uidNumber, game)).setEphemeral(true).queue();

        } else if (event.getName().equals("cards")) {

            Optional<HoyoDetails> hoyoDetails = hoyoDetailsRepo.findById(userId);

            if (hoyoDetails.isEmpty()) {
                event.reply("You haven't linked your any accounts yet.").queue();
                return;
            }

            List<Uid> uidList = hoyoDetails.get().getUidList();
            Optional<Uid> uid = uidList.stream().filter(u -> u.getGame() == Game.STARRAIL).findFirst();

            if (uid.isEmpty()) {
                event.reply("No UIDs found for Starrail").queue();
                return;
            }


            File userDataFolder = new File(cardsConfig.getDataFolder() + "/" + uid.get().getUidNumber());
            if (userDataFolder.isDirectory()) {
                File[] cardFiles = userDataFolder.listFiles();
                assert cardFiles != null;
                if (cardFiles.length < 1) {
                     event.reply("Still generating, check back later.").queue();
                     return;
                }

                StringSelectMenu.Builder selectMenu = StringSelectMenu.create("choose_chara");
                for (File f : cardFiles) {
                    String chara = f.getName();
                    String onlyName = chara.substring(0, chara.length() - 4);
                    selectMenu.addOption(onlyName, chara, onlyName);
                }

                event.reply("Select your character")
                        .addActionRow(selectMenu.build())
                        .queue();
            } else {

                event.reply("First time generation, check back later").queue();

                try {
                    ObjectMapper objectMapper = new ObjectMapper();
                    List registeredUids  = objectMapper.readValue(new File(cardsConfig.getUidList()), List.class);
                    registeredUids.add(uid.get().getUidNumber());
                    registeredUids = registeredUids.stream().distinct().toList();
                    objectMapper.writeValue(new File(cardsConfig.getUidList()), registeredUids);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }


    @Override
    public void onStringSelectInteraction(StringSelectInteractionEvent event) {

        Optional<HoyoDetails> hoyoDetails = hoyoDetailsRepo.findById(event.getUser().getId());

        if (hoyoDetails.isEmpty()) {
            event.reply("You haven't linked your any UIDs yet.").queue();
            return;
        }

        List<Uid> uidList = hoyoDetails.get().getUidList();
        Optional<Uid> uid = uidList.stream().filter(u -> u.getGame() == Game.STARRAIL).findFirst();

        if (uid.isEmpty()) {
            event.reply("No UIDs found for Starrail").queue();
            return;
        }

        if (event.getComponentId().equals("choose_chara")) {
//            event.editMessageAttachments(FileUpload.fromData(new File(
//                    cardsConfig.getDataFolder() + "/"
//                            + uid.get().getUidNumber() + "/"
//                            + event.getValues().get(0)))).queue();


            event.editMessageAttachments(FileUpload.fromData(new File(
                    cardsConfig.getDataFolder() + "/"
                            + uid.get().getUidNumber() + "/"
                            + event.getValues().get(0)))).queue();
        }
    }
}


package in.sliya.shuri.commands.redeem;

import in.sliya.shuri.modules.genshin.model.HoyoCreds;
import in.sliya.shuri.modules.redeem.dao.HoyoCredsDao;
import lombok.NonNull;
import net.dv8tion.jda.api.events.interaction.ModalInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContinueRegisterUser extends ListenerAdapter  {

    @Autowired
    HoyoCredsDao dao;

    @Override
    public void onModalInteraction(@NonNull ModalInteractionEvent event) {


        if(!event.getModalId().equals("register_redeem")) {
           return;
        }

        String username = event.getValue("username").getAsString();
        String password = event.getValue("password").getAsString();

        HoyoCreds credentials = new HoyoCreds(event.getUser().getId(), username, password);
        dao.saveCreds(credentials);
        event.reply("saved successful").setEphemeral(true).queue();
    }
}

package in.sliya.shuri.commands.redeem;


import in.sliya.shuri.base.CmdHandler;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.text.TextInput;
import net.dv8tion.jda.api.interactions.components.text.TextInputStyle;
import net.dv8tion.jda.api.interactions.modals.Modal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


// TODO: this should be a system command to overthrow all delays and report much better ping.
@Component("Redeem Command")
public class RegisterUser extends CmdHandler {

    private final Logger logger = LoggerFactory.getLogger(RegisterUser.class);

    RegisterUser() {
        setCommandName("register");
    }

    @Override
    public void handleSlash(SlashCommandInteractionEvent event) {


        TextInput cookie = TextInput.create("uid", "Genshin UID", TextInputStyle.SHORT)
                .setPlaceholder("password")
                .setMinLength(5)
                .setMaxLength(12)
                .build();

//        TextInput uid = TextInput.create("uid", "Genshin UID", TextInputStyle.SHORT)
//                .setPlaceholder("password")
//                .setMinLength(5)
//                .setMaxLength(12)
//                .build();
//        TextInput username = TextInput.create("username", "Username", TextInputStyle.SHORT)
//                .setPlaceholder("Subject of this ticket")
//                .setMinLength(3)
//                .setMaxLength(50) // or setRequiredRange(10, 100)
//                .build();
//
//        TextInput password = TextInput.create("password", "Password", TextInputStyle.SHORT)
//                .setPlaceholder("password")
//                .setMinLength(3)
//                .setMaxLength(50)
//                .build();
//
        Modal modal = Modal.create("register_redeem", "Hoyo Authentication")
                .addComponents(ActionRow.of(cookie))
                .build();

        event.replyModal(modal).queue();



    }
}

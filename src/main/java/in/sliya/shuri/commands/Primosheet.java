package in.sliya.shuri.commands;


import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import in.sliya.shuri.base.MessageCommand;
import in.sliya.shuri.base.CmdHandler;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.utils.FileUpload;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static in.sliya.shuri.modules.primosheet.Primosheet.getCredentials;


// TODO: this should be a system command to overthrow all delays and report much better ping.
@Component
public class Primosheet extends CmdHandler {

    private final Logger logger = LoggerFactory.getLogger(Primosheet.class);

    private static final String APPLICATION_NAME = "Google Sheets API Java Quickstart";
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES =
            Collections.singletonList(SheetsScopes.SPREADSHEETS_READONLY);
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";


    Primosheet() {
        setCommandName("primo");
    }


    @Override
    public void handleMsg(MessageCommand cmd, MessageReceivedEvent event) {
        final String spreadsheetId = "1ltJksS03osS5kr4qn9ZZa3U5vJM-OqC2I1pdGe_LF7k";
        final String range = "earnings!A4:B15";

        Optional<NetHttpTransport> transport = getTransport();

        if (transport.isEmpty()) {
            // error and return
            return;
        }

        NetHttpTransport HTTP_TRANSPORT = transport.get();


        ValueRange response;
        try {

            Sheets service =
                    new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                            .setApplicationName(APPLICATION_NAME)
                            .build();

            response = service.spreadsheets().values()
                    .get(spreadsheetId, range)
                    .execute();
        } catch (IOException e) {
            // io error occured. just ignore and go away.
            logger.error("exception occured: ", e);
            return;
        }


        List<List<Object>> values = response.getValues();

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        if (values == null || values.isEmpty()) {
            // no data found.
        } else {
            values.stream().sorted((x, y) -> {
                Double left = Double.parseDouble((String) x.get(1));
                Double right = Double.parseDouble((String) y.get(1));
                return right.compareTo(left);
            }).forEach(row -> {

                dataset.addValue(Double.parseDouble((String) row.get(1)), "Label", (String) row.get(0) + " (" + (String) row.get(1) + ")");
            });
        }


        // Create a pie chart
        JFreeChart chart = ChartFactory.createBarChart(
                "Primogems Breakdown", //Chart Title
                "Source", // Category axis
                "Primogems", // Value axis
                dataset,
                PlotOrientation.HORIZONTAL,
                false, true, false
        );


        int width = 800;    // Width of the image
        int height = 500;   // Height of the image
        try {
            File imageFile = new File("primos.jpg");
            ChartUtils.saveChartAsJPEG(imageFile, chart, width, height);
            logger.info("chart generated successfully");
        } catch (IOException e) {
            logger.error("exception occurred while saving chart.", e);
        }
        event.getChannel().sendFiles(FileUpload.fromData(new File("primos.jpg"))).queue();


    }

    private Optional<NetHttpTransport> getTransport() {
        try {
            return Optional.of(GoogleNetHttpTransport.newTrustedTransport());

        } catch (IOException | GeneralSecurityException e) {
            logger.error("exception occurred while saving chart.", e);
            return Optional.empty();
        }
    }
}


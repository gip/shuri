package in.sliya.shuri.modules.redeem.dao;

import in.sliya.shuri.modules.genshin.model.HoyoCreds;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext
class HoyoCredsDaoTest {

    static final Logger logger = LoggerFactory.getLogger(HoyoCreds.class);

    @Autowired
    HoyoCredsDao dao;

    @Test
    void upsertCredentialsTest() {

        HoyoCreds credentials = new HoyoCreds("discordUserId", "c9ss6kg99@mozmail.com", "Furuhashi");
        dao.saveCreds(credentials);

        HoyoCreds retrievedCredentials = dao.getCredsById("discordUserId");

        logger.info("retrieved creds: {}", retrievedCredentials);

    }


}
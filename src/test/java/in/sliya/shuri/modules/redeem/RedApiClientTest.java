package in.sliya.shuri.modules.redeem;

import in.sliya.shuri.modules.genshin.model.Region;
import in.sliya.shuri.modules.genshin.model.HoyoCreds;
import in.sliya.shuri.modules.redeem.dao.HoyoCredsDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.transaction.Transactional;

@SpringBootTest
class RedApiClientTest {


    @Autowired
    RedApiClient redApiClient;

    @Autowired
    HoyoCredsDao dao;

    @Test
    @DirtiesContext
    @Transactional
    void getCookiesTest() {
        String uid = "812786494";
         redApiClient.refreshCookies(
                new HoyoCreds("discordUserId", "abc", "def"));

        assert(dao.getCredsById(uid).getCookie_token_v2() != null);

    }


    @Test
    void redeemCodeTest() {

        HoyoCreds creds = new HoyoCreds("discordUserId", "bobyclSDaws1", "FuruDDhashi");
//        RedCreds creds = dao.getCredentialsByUid("812786494");

        dao.saveCreds(creds);
        System.out.println(redApiClient.redeemCode(null, "GENSHINGIFT", Region.ASIA));

    }

}
package in.sliya.shuri.modules.enka;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.ws.rs.client.Client;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EnkaClientTest {

    @Autowired
    EnkaClient client;

    @Test
    void validateUidNum() {

       assertTrue(client.validateUidNum("812786494"));
    }
}
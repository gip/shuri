package in.sliya.shuri.modules.genshin;

import in.sliya.shuri.modules.genshin.model.Game;
import in.sliya.shuri.modules.genshin.model.Uid;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@SpringBootTest
class GeneralApiClientTest {

    @Autowired
    GeneralApiClient client;
    @Test
    void getRoles() {

        Map<Game, List<Uid>> uidList = client.getUidList();

        for (Game key: uidList.keySet()) {
            for (Uid val: uidList.get(key)) {
                System.out.println("key: " + key + ", val: " + val);
            }
        }
    }
}
package in.sliya.shuri.modules.akashacv;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AkashCVTest {

    private static final Logger logger = LoggerFactory.getLogger(AkashCVTest.class);

    @Test
    void getCalculations() {

        AkashCV ak = new AkashCV();
        String uid = "812786494";
        UserCalculation userCalculation = ak.getUserCalculation(uid);
        List<Character> characterList = userCalculation.characterList;
        byte ranking = characterList.get(0).calculations.getRanking();
        String name = characterList.get(0).name;
        logger.info("calculations: top {}% :{}", ranking * 100, name);

    }
}
package in.sliya.shuri.mocks;

import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.channel.unions.MessageChannelUnion;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageCreateAction;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Mocks {

    public static MessageReceivedEvent mockMessageCreateEvent() {
        User mockUser = mock(User.class);
        MessageChannelUnion mockMessageChannelUnion =  mock(MessageChannelUnion.class);
        MessageCreateAction mockMessageCreateAction = mock(MessageCreateAction.class);
        MessageReceivedEvent mockMessageReceivedEvent = mock(MessageReceivedEvent.class);

        when(mockMessageReceivedEvent.getAuthor())
                .thenReturn(mockUser);
        when(mockUser.getName())
                .thenReturn("TestUser");

        when(mockMessageReceivedEvent.getChannel())
                .thenReturn(mockMessageChannelUnion);
        when(mockMessageChannelUnion.sendMessage((CharSequence) any()))
                .thenReturn(mockMessageCreateAction);

        return mockMessageReceivedEvent;
    }
}
